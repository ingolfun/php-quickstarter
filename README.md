# PHP QuickStarter

> Dockerized PHP development stack: Nginx, MySQL, PHP-FPM, Memcached (Based on [PHP Dockerized](https://github.com/kasperisager/php-dockerized))

## What's inside

* [Nginx](http://nginx.org/)
* [MySQL](http://www.mysql.com/)
* [PHP-FPM](http://php-fpm.org/)
* [Memcached](http://memcached.org/)

## Requirements

* [Docker Engine](https://docs.docker.com/installation/)
* [Docker Compose](https://docs.docker.com/compose/)
* [Docker Machine](https://docs.docker.com/machine/) (Mac and Windows only)

## Running

Set up a Docker Machine and then run:

```sh
$ sudo docker-compose up -d
```

That's it! You can now access your configured sites via the IP address of the Docker Machine or locally if you're running a Linux flavour and using Docker natively.

## License

Copyright &copy; 2023 [Ingolf](http://gitlab.com/ingolfun). Licensed under the terms of the [MIT license](LICENSE.md).
